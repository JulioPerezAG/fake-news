const express = require('express');
const newsRouter = express.Router();
const httpStatusCodes = require('http-status-codes');
const firebaseConnection = require('../common/firebase-connection');
const language = require('@google-cloud/language');
const client = new language.LanguageServiceClient();
const _ = require('lodash');
const crypto = require('crypto');

/**
 *  Method to generate tokens at the database
 */
/* for (let index = 0; index < 100; index++) {
    crypto.randomBytes(48, function (err, buffer) {
        var value = buffer.toString('hex');
        var database = firebaseConnection
            .database().ref('tokens').push({ value }).then(response => {
                console.log({ value });
            });
    })
} */

newsRouter.post('/save-news', function (req, res) {
    var { news, token, userEmail } = req.body;

    if (!token) {
        res.status(httpStatusCodes.BAD_REQUEST);
        res.json({ 'error': 'The token property is mandatory' });
    }

    if (!news) {
        res.status(httpStatusCodes.BAD_REQUEST);
        res.json({ 'error': 'The news property is mandatory' });
    }

    if (!userEmail) {
        res.status(httpStatusCodes.BAD_REQUEST);
        res.json({ 'error': 'The userEmail property is mandatory' });
    }

    var verifyToken = null;
    var tokenKey = null;
    firebaseConnection.database()
        .ref('tokens').orderByChild('value').equalTo(token).once('value', (snapshot) => {
            verifyToken = snapshot.val();
            snapshot.forEach((data) => {
                tokenKey = data.key
            });
            if (!verifyToken) {
                res.status(httpStatusCodes.UNAUTHORIZED);
                res.send({ 'error': 'Error, please verify that the token is correct' });
                res.end();
            } else {
                var document = {
                    content: news.text,
                    type: 'PLAIN_TEXT'
                };

                client.analyzeEntities({ document }).then(results => {
                    const entities = results[0].entities;
                    var entitiesNames = [];
                    entities.forEach(entity => {
                        entitiesNames.push(entity.name);
                    });
                    entitiesNames = _.uniq(entitiesNames);
                    news.tags = news.tags.concat(entitiesNames);
                    var database = firebaseConnection.database();
                    database.ref('projects').push(req.body).then((response) => {
                        database.ref('tokens/' + tokenKey).remove()
                            .then(() => {
                                res.status(httpStatusCodes.OK);
                                res.send();
                            }, err => {
                                res.status(httpStatusCodes.INTERNAL_SERVER_ERROR);
                                res.send({ 'error': err });
                            })
                    }, (err) => {
                        res.status(httpStatusCodes.INTERNAL_SERVER_ERROR);
                        res.send({ 'error': err });
                    });
                }).catch(err => {
                    res.status(httpStatusCodes.INTERNAL_SERVER_ERROR);
                    res.send({ 'error2': err });
                });
            }
        })

});

module.exports = newsRouter;