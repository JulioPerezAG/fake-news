var express = require('express');
var imageRouter = express.Router();
var fs = require('file-system');
const vision = require('@google-cloud/vision');
const client = new vision.ImageAnnotatorClient();
var httpStatusCodes = require('http-status-codes');

imageRouter.post('/upload', function (req, res) {
  var { imageUrl } = req.body;

  if (!imageUrl)
    res.json({ 'error': 'The name and the extension of the file is mandatory' });

  /*var creationDate = new Date();
  var finalName = fileName + '' + creationDate.getTime() + '.' + fileExt;
  var { data } = req.body;

  if (!data)
    res.json({ "error": "Is necesary upload a image" });

  var finalImage = data.replace(/^data:image\/\w+;base64,/, "");
  var buf = new Buffer(finalImage, 'base64');
  fs.writeFile(finalName, buf);*/

  console.log(imageUrl);
  var imageRelatedData = {};

  client.webDetection(imageUrl).then(results => {
    const webDetection = results[0].webDetection;
    console.log(results);
    if (webDetection.fullMatchingImages.length) {
      imageRelatedData.fullMatchingImages = [];
      webDetection.fullMatchingImages.forEach(image => {
        imageRelatedData.fullMatchingImages.push(
          { 'url': image.url, 'score': image.score, 'listType' : 'grayList' }
        );
      });
    }

    if (webDetection.partialMatchingImages.length) {
      imageRelatedData.partialMatchingImages = [];

      webDetection.partialMatchingImages.forEach(image => {
        imageRelatedData.partialMatchingImages.push(
          { 'url': image.url, 'score': image.score, 'listType' : 'grayList' }
        );
      });
    }

    res.json({ 'data': imageRelatedData });
  }).catch(err => {
    console.log(err);
    res.status(httpStatusCodes.BAD_REQUEST);
    res.json({ 'error': 'The error is here' })
  });
})

module.exports = imageRouter;
