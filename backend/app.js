var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var usersRouter = require('./routes/users');
var authRouter = require('./routes/auth');
var imageRouter = require('./routes/image');
var newsRouter = require('./routes/saveNews');

app.use(function (req, response, next) {
	response.setHeader("Access-Control-Allow-Origin", "*");
	response.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
	response.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
	next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8086;

var router = express.Router();

router.get('/', function (req, res) {
	res.json({ message: '*Welcome to the Fake News API*' });
});

app.use('/api/news', newsRouter);
app.use('/api/image/', imageRouter);
app.use('/api', router);
app.use('/api/users', usersRouter);
app.use('/api/auth', authRouter);

app.listen(port);
console.log('Magic happens on port :', port);
