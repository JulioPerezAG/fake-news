import {Injectable} from '@angular/core';

@Injectable()
export class UserService {

  get user() {
    let _user = JSON.parse(localStorage.getItem('user'));
    if (_user) {
      _user = _user.data;
    }
    if (_user) {
      _user = _user.user;
    }
    return _user;
  }

  set user(user: any) {
    localStorage.setItem('user', JSON.stringify(user));
  }

  removeUser() {
    localStorage.clear();
  }
}
