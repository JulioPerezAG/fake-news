import {Directive, Input, Renderer2} from '@angular/core';

@Directive({
  selector: '[appIntrusiveLoading]'
})
export class IntrusiveLoadingDirective {

  constructor(private renderer: Renderer2) {
  }

  @Input('appIntrusiveLoading') set disableScrolling(disable: boolean) {
    this.renderer[disable ? 'addClass' : 'removeClass'](document.body, 'disable-scroll');
  }
}
