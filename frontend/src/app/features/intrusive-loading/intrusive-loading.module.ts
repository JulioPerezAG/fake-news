import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {IntrusiveLoadingComponent} from './component/intrusive-loading.component';

import {IntrusiveLoadingDirective} from './directives/intrusive-loading.directive';

import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    IntrusiveLoadingComponent,
    IntrusiveLoadingDirective
  ],
  imports: [
    CommonModule,
    FontAwesomeModule
  ],
  exports: [
    IntrusiveLoadingComponent,
    IntrusiveLoadingDirective
  ]
})
export class IntrusiveLoadingModule {
}
