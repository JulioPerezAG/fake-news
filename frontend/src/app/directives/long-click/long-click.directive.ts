import {Directive, EventEmitter, HostListener, Input, Output} from '@angular/core';
import Timer = NodeJS.Timer;

@Directive({
  selector: '[appLongClick]'
})
export class LongClickDirective {

  @Input() refreshRate = 25;

  @Output() longClick: EventEmitter<Event>;

  private emulateLongClick: Timer;

  constructor() {
    this.longClick = new EventEmitter<Event>();
  }

  @HostListener('mousedown', ['$event'])
  onMouseDown(event: Event) {
    this.emulateLongClick = setInterval(() => this.longClick.emit(event), this.refreshRate);
  }

  @HostListener('mouseup', ['$event'])
  onMouseUp(event: Event) {
    clearInterval(this.emulateLongClick);
    this.longClick.emit(event);
  }
}
