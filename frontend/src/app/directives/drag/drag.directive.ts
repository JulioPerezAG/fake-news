import {Directive, EventEmitter, HostListener, Output} from '@angular/core';

@Directive({
  selector: '[appDrag]'
})
export class DragDirective {

  @Output() dragOnX: EventEmitter<number>;

  @Output() dragStart: EventEmitter<Event>;

  @Output() dragEnd: EventEmitter<Event>;

  private screenXStart: number;

  constructor() {
    this.dragOnX = new EventEmitter<number>();
    this.dragStart = new EventEmitter<Event>();
    this.dragEnd = new EventEmitter<Event>();
  }

  @HostListener('dragstart', ['$event', '$event.screenX'])
  onDragStart(e: Event, x: number) {
    this.screenXStart = x;
    this.dragStart.emit(e);
  }

  @HostListener('drag', ['$event.screenX'])
  onDrag(x: number) {
    if (x) {
      this.dragOnX.emit(x - this.screenXStart);
    }
  }

  @HostListener('dragend', ['$event'])
  onDragEnd(e: Event) {
    this.dragEnd.emit(e);
  }
}
