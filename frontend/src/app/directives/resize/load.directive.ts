import {Directive, ElementRef, EventEmitter, HostListener, Output} from '@angular/core';

@Directive({
  selector: '[appLoad]'
})
export class LoadDirective {

  @Output() loadSource: EventEmitter<Event>;

  @Output() errorLoadingSource: EventEmitter<any>;

  constructor(private element: ElementRef) {
    this.loadSource = new EventEmitter<Event>();
    this.errorLoadingSource = new EventEmitter<any>();
    this.element.nativeElement.addEventListener('error', this.onError());
  }

  @HostListener('load', ['$event'])
  onResize(e: Event) {
    this.loadSource.emit(e);
  }

  onError() {
    this.errorLoadingSource.emit();
  }
}
