import {NgModule} from '@angular/core';

import {LoadDirective} from './load.directive';

@NgModule({
  declarations: [
    LoadDirective
  ],
  imports: [],
  exports: [
    LoadDirective
  ]
})
export class LoadModule {

}
