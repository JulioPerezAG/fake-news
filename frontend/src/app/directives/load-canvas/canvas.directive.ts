import {Directive, ElementRef, OnInit, Renderer2} from '@angular/core';
import {Observable, Observer} from 'rxjs';

@Directive({
  selector: '[appCanvas]'
})
export class CanvasDirective {

  renderingContext: CanvasRenderingContext2D;

  constructor(private container: ElementRef,
              private renderer: Renderer2) {
  }

  loadFromBase64(base64: string): Observable<string> {
    return Observable.create((observer: Observer<string>) => {
      const canvas = this.renderer.createComment('canvas');
      console.log(canvas);
      /*this.renderingContext = this.canvas.nativeElement.getContext('2d');
      const image = new Image();
      image.onload = () => {
        this.renderingContext.drawImage(image, 0, 0);
        observer.next(canvas.nativeElement.toDataURL());
      };
      image.onerror = () => observer.error('');
      image.src = base64;*/
    });
  }
}
