import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';

import {options, routes} from './app.routes';

import {AuthGuard} from './guards/auth.guard';
import {UserService} from './services/user.service';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {ToastrModule} from 'ngx-toastr';

import * as firebase from 'firebase';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes, options),
    HttpClientModule,
    NgbModule.forRoot(),
    ToastrModule.forRoot()
  ],
  providers: [
    AuthGuard,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  config = {
    apiKey: 'AIzaSyBkPfC_TANshpe234GAkzHsjebcXPHqIsI',
    storageBucket: 'fake-news-v2.appspot.com'
  };

  constructor() {
    firebase.initializeApp(this.config);
  }
}
