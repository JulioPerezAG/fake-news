import * as firebase from 'firebase';
import TaskState = firebase.storage.TaskState;

export interface FirebaseTaskStatus {
  status: TaskState | 'COMPLETE';
  progress?: number;
  downloadUrl?: string;
}
