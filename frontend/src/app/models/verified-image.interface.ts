import {ListType} from './list-type.enum';

export interface VerifiedImage {
  url?: string;
  score?: number;
  listType?: ListType;
}
