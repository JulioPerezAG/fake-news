import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FileTableComponent} from './file-table.component';
import {FileRowComponent} from './file-row.component';

@NgModule({
  declarations: [
    FileTableComponent,
    FileRowComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    FileTableComponent
  ]
})
export class FileTableModule {

}
