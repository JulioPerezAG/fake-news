import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {faUser} from '@fortawesome/free-solid-svg-icons/faUser';
import {faUnlockAlt} from '@fortawesome/free-solid-svg-icons/faUnlockAlt';
import {LoginForm} from '../../models/login-form.interface';
import {faSpinner} from '@fortawesome/free-solid-svg-icons/faSpinner';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  @Input() loading: boolean;

  @Output() attemptLogin: EventEmitter<LoginForm>;

  user = faUser;

  password = faUnlockAlt;

  form: FormGroup;

  loadingIcon = faSpinner;

  constructor(formBuilder: FormBuilder) {
    this.form = formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      rememberMe: false
    });

    this.attemptLogin = new EventEmitter<LoginForm>();
  }

  onAttemptLogin() {
    this.attemptLogin.emit(this.form.value);
  }
}
