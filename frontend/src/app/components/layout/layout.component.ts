import {Component, EventEmitter, Input, Output, TemplateRef} from '@angular/core';

import {faInstagram} from '@fortawesome/free-brands-svg-icons/faInstagram';
import {faFacebookF} from '@fortawesome/free-brands-svg-icons/faFacebookF';
import {faTwitter} from '@fortawesome/free-brands-svg-icons/faTwitter';
import {faCircle} from '@fortawesome/free-regular-svg-icons/faCircle';
import {faEllipsisV} from '@fortawesome/free-solid-svg-icons/faEllipsisV';
import {faPowerOff} from '@fortawesome/free-solid-svg-icons/faPowerOff';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent {

  @Input() templateOutlet: TemplateRef<any>;

  @Input() context: Object;

  @Input() username: string;

  @Input() email: string;

  @Output() logout: EventEmitter<void>;

  @Input() selectedScreen: string;

  instagram = faInstagram;

  facebook = faFacebookF;

  twitter = faTwitter;

  circle = faCircle;

  dots = faEllipsisV;

  powerOff = faPowerOff;

  constructor() {
    this.logout = new EventEmitter<void>();
  }

  onLogout() {
    this.logout.emit();
  }
}
