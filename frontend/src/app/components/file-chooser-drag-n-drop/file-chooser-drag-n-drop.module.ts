import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FileChooserDragNDropComponent} from './file-chooser-drag-n-drop.component';

import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

@NgModule({
    declarations: [
        FileChooserDragNDropComponent
    ],
    imports: [
        CommonModule,
        FontAwesomeModule
    ],
    exports: [
        FileChooserDragNDropComponent
    ]
})
export class FileChooserDragNDropModule {

}
