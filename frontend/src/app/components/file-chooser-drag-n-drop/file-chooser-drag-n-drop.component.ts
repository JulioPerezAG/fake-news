import {Component, EventEmitter, Input, Output} from '@angular/core';

import {faPlus} from '@fortawesome/free-solid-svg-icons/faPlus';
import {faSpinner} from '@fortawesome/free-solid-svg-icons/faSpinner';

@Component({
  selector: 'app-file-chooser-drag-n-drop',
  templateUrl: './file-chooser-drag-n-drop.component.html',
  styleUrls: ['./file-chooser-drag-n-drop.component.scss']
})
export class FileChooserDragNDropComponent {

  @Input() accept: string[];

  @Input() max: number;

  @Input() loading: boolean;

  @Output() chooseFile: EventEmitter<File>;

  @Output() fileError: EventEmitter<string>;

  plus = faPlus;

  loadingIcon = faSpinner;

  active: boolean;

  error: boolean;

  target: EventTarget;

  constructor() {
    this.chooseFile = new EventEmitter<File>();
    this.fileError = new EventEmitter<string>();
  }

  onSelected(file: any) {
    if (this.loading) {
      return;
    }
    if (!file) {
      return;
    }

    if (this.accept && this.accept.some(value => value.includes(file.type.split('/')[1]))) {
      this.chooseFile.emit(file);
    } else {
      this.error = true;
      setTimeout(() => this.error = false, 1000);
      this.fileError.emit('INVALID_TYPE');
    }
  }

  onDrop(e: DragEvent) {
    if (this.loading) {
      return;
    }
    e.preventDefault();
    this.active = false;
    if (e.dataTransfer.items) {
      // Use DataTransferItemList interface to access the file(s)
      for (let i = 0; i < e.dataTransfer.items.length; i++) {
        // If dropped items aren't files, reject them
        if (e.dataTransfer.items[i].kind === 'file') {
          this.onSelected(e.dataTransfer.items[i].getAsFile());
        }
      }
    } else {
      // Use DataTransfer interface to access the file(s)
      for (let i = 0; i < e.dataTransfer.files.length; i++) {
        this.onSelected(e.dataTransfer.files[i].name);
      }
    }
    this.removeDragData(e);
  }

  onDragEnter(e: DragEvent) {
    this.target = e.target;
    this.active = true;
  }

  onDragLeave(e: Event) {
    if (this.target === e.target) {
      this.active = false;
    }
  }

  onDragOver(e: Event): boolean {
    return false;
  }

  removeDragData(ev) {
    if (ev.dataTransfer.items) {
      // Use DataTransferItemList interface to remove the drag data
      ev.dataTransfer.items.clear();
    } else {
      // Use DataTransfer interface to remove the drag data
      ev.dataTransfer.clearData();
    }
  }
}
