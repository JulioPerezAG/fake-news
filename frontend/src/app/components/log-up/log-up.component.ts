import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {PasswordValidators} from '../../validators/password/password.validators';

import {faUser} from '@fortawesome/free-solid-svg-icons/faUser';
import {faUnlockAlt} from '@fortawesome/free-solid-svg-icons/faUnlockAlt';
import {faEnvelope} from '@fortawesome/free-solid-svg-icons/faEnvelope';
import {faEdit} from '@fortawesome/free-solid-svg-icons/faEdit';
import {faBuilding} from '@fortawesome/free-solid-svg-icons/faBuilding';
import {faSpinner} from '@fortawesome/free-solid-svg-icons/faSpinner';
import {LogUpForm} from '../../models/log-up-form.interface';

@Component({
  selector: 'app-log-up',
  templateUrl: './log-up.component.html',
  styleUrls: ['./log-up.component.scss']
})
export class LogUpComponent implements OnInit {

  @Input() loading: boolean;

  @Output() size: EventEmitter<string>;

  @Output() attemptLogUp: EventEmitter<LogUpForm>;

  enableToken: boolean;

  user = faUser;

  loadingIcon = faSpinner;

  email = faEnvelope;

  password = faUnlockAlt;

  company = faBuilding;

  token = faEdit;

  form: FormGroup;

  constructor(formBuilder: FormBuilder) {
    this.form = formBuilder.group({
      name: ['', Validators.required],
      firstLastName: ['', Validators.required],
      secondLastName: '',
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      company: [''],
      token: [{value: '', disabled: true}, Validators.required],
      subscribeToNews: true
    }, {
      validator: PasswordValidators
        .matchPassword('password', 'confirmPassword')
    });

    this.size = new EventEmitter<string>();
    this.attemptLogUp = new EventEmitter<LogUpForm>();
  }

  ngOnInit(): void {
    this.size.emit('sm');
  }

  onClickEnableTokenInput(): void {
    this.form.controls.token.enable();
    this.enableToken = true;
    this.size.emit('xl');
  }

  onAttemptLogUp() {
    this.attemptLogUp.emit(this.form.value);
  }
}
