import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';

import {LogUpComponent} from './log-up.component';

import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

import {PCheckboxModule} from '../p-checkbox/p-checkbox.module';

@NgModule({
  declarations: [
    LogUpComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    PCheckboxModule
  ],
  exports: [
    LogUpComponent
  ]
})
export class LogUpModule {

}
