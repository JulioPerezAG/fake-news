import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import {PCheckboxComponent} from './p-checkbox.component';

@NgModule({
  declarations: [
    PCheckboxComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    PCheckboxComponent
  ]
})
export class PCheckboxModule {
}
