import {Component, forwardRef, Input, OnInit} from '@angular/core';
import {ControlValueAccessor, FormBuilder, FormGroup, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'app-p-checkbox',
  templateUrl: './p-checkbox.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PCheckboxComponent),
      multi: true
    }
  ]
})
export class PCheckboxComponent implements ControlValueAccessor, OnInit {

  @Input() text: string;

  form: FormGroup;

  change: Function;

  constructor(formBuilder: FormBuilder) {
    this.form = formBuilder.group({pCheckBox: false});
  }

  ngOnInit(): void {
    this.form.controls.pCheckBox.valueChanges.subscribe(value => this.change ? this.change(value) : '');
  }

  registerOnChange(fn: Function): void {
    this.change = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
    this.form[isDisabled ? 'disable' : 'enable']();
  }

  writeValue(obj: boolean): void {
    this.form.controls.pCheckBox.patchValue(obj);
  }
}
