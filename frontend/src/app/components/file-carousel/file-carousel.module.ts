import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FileCarouselComponent} from './file-carousel.component';
import {FileCarouselItemComponent} from './file-carousel-item.component';

import {DragModule} from '../../directives/drag/drag.module';
import {LoadModule} from '../../directives/resize/load.module';
import {LongClickModule} from '../../directives/long-click/long-click.module';
import {DomSanitizerModule} from '../../pipes/dom-sanitizer/dom-sanitizer.module';

import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

@NgModule({
  declarations: [
    FileCarouselComponent,
    FileCarouselItemComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    DragModule,
    LoadModule,
    LongClickModule,
    DomSanitizerModule
  ],
  exports: [
    FileCarouselComponent
  ]
})
export class FileCarouselModule {

}
