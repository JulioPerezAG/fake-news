import {Component, ElementRef, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {FileCarousel} from '../../models/file-carousel.interface';

@Component({
  selector: '[app-file-carousel-item]',
  templateUrl: './file-carousel-item.component.html'
})
export class FileCarouselItemComponent {

  @Input() image: FileCarousel;

  @Input() index: number;

  @Input() selected: number;

  @Output() itemSelected: EventEmitter<number>;

  @Output() delete: EventEmitter<number>;

  @Output() cardSize: EventEmitter<number>;

  @ViewChild('card') card: ElementRef;

  constructor() {
    this.itemSelected = new EventEmitter<number>();
    this.delete = new EventEmitter<number>();
    this.cardSize = new EventEmitter<number>();
  }

  get width(): number {
    return this.card.nativeElement.offsetWidth;
  }

  get height(): number {
    return this.card.nativeElement.offsetHeight;
  }

  onLoad() {
    this.cardSize.emit(this.card.nativeElement.offsetWidth);
  }

  onClick() {
    this.itemSelected.emit(this.index);
  }

  onDelete() {
    this.delete.emit(this.index);
  }

  onError() {
    console.log('error');
    this.onDelete();
  }
}
