import {CanLoad, Route, Router} from '@angular/router';
import {Injectable} from '@angular/core';

import {Observable} from 'rxjs';

import {UserService} from '../services/user.service';

@Injectable()
export class AuthGuard implements CanLoad {

  constructor(private userService: UserService, private router: Router) {
  }

  canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
    if (route.path.includes('login')) {
      if (!this.userService.user) {
        return true;
      } else {
        this.router.navigate(['home']);
        return false;
      }
    } else if (route.path.includes('home')) {
      if (this.userService.user) {
        return true;
      } else {
        this.router.navigate(['login']);
        return false;
      }
    }
  }
}
