import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {VerifyTokenViewComponent} from './verify-token-view.component';

import {VerifyService} from '../../services/verify.service';
import {ImageService} from '../../services/image.service';

import {FileChooserDragNDropModule} from '../../components/file-chooser-drag-n-drop/file-chooser-drag-n-drop.module';
import {FileTableModule} from '../../components/file-table/file-table.module';
import {FileCarouselModule} from '../../components/file-carousel/file-carousel.module';

import {CanvasModule} from '../../directives/load-canvas/canvas.module';

@NgModule({
  declarations: [
    VerifyTokenViewComponent
  ],
  imports: [
    CommonModule,
    FileChooserDragNDropModule,
    FileTableModule,
    FileCarouselModule,
    CanvasModule
  ],
  exports: [
    VerifyTokenViewComponent
  ],
  providers: [
    VerifyService,
    ImageService
  ]
})
export class VerifyTokenViewModule {

}
