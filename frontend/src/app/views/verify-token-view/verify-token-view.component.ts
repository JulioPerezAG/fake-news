import {Component, ViewChild} from '@angular/core';

import {FileCarousel} from '../../models/file-carousel.interface';
import {VerifiedImages} from '../../models/verified-images.interface';

import {VerifyService} from '../../services/verify.service';
import {ImageService} from '../../services/image.service';
import {CanvasDirective} from '../../directives/load-canvas/canvas.directive';
import {filter, finalize} from 'rxjs/operators';
import {FirebaseTaskStatus} from '../../models/firebase-task-status.interface';

@Component({
  selector: 'app-verify-token-view',
  templateUrl: './verify-token-view.component.html'
})
export class VerifyTokenViewComponent {

  @ViewChild(CanvasDirective) loadCanvasDirective: CanvasDirective;

  images: FileCarousel[];

  selectedImage: VerifiedImages;

  loading: boolean;

  selectedImageIndex: number;

  constructor(private verifyService: VerifyService,
              private imageService: ImageService) {
    this.images = [];
  }

  onDelete(item: number) {
    this.images = this.images.filter((image: FileCarousel, index: number) => index !== item);
    this.selectedImage = null;
  }

  onChooseFile(file: File) {
    this.loading = true;
    this.verifyService.uploadImage(file)
      .pipe(filter((value: FirebaseTaskStatus) => value.status === 'COMPLETE'))
      .subscribe(status =>
        this.imageService.imageToBase64(file)
          .subscribe(image => {
            this.verifyService.verifyImage({
              imageUrl: status.downloadUrl
            }).pipe(finalize(() => this.loading = false))
              .subscribe(next => {
                this.images.push({data: next, image: image});
                this.onChooseImage(this.images.length - 1);
                this.selectedImageIndex = this.images.length - 1;
              });
          }, () => this.loading = false), () => this.loading = false);
  }

  onChooseImage(item: number) {
    this.selectedImage = this.images[item].data;
  }
}
