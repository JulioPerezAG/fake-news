import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-layout-view',
  templateUrl: './layout-view.component.html'
})
export class LayoutViewComponent implements OnInit {

  view: string;

  verifyToken = 'verify-token';

  user: any;

  constructor(private activateRoute: ActivatedRoute,
              private router: Router,
              private userService: UserService) {
    const view = activateRoute.snapshot.paramMap.get('view');
    if (!view) {
      router.navigate(['home', this.verifyToken]);
    } else {
      this.view = view;
    }
  }

  ngOnInit(): void {
    this.user = this.userService.user;
  }

  onLogout() {
    this.userService.removeUser();
    this.router.navigate(['login']);
  }
}
