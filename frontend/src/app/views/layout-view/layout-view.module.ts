import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {routes} from './layout-view.routes';

import {LayoutViewComponent} from './layout-view.component';

import {LayoutModule} from '../../components/layout/layout.module';
import {VerifyTokenViewModule} from '../verify-token-view/verify-token-view.module';

@NgModule({
    declarations: [
        LayoutViewComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        LayoutModule,
        VerifyTokenViewModule
    ],
    exports: [
        LayoutViewComponent
    ]
})
export class LayoutViewModule {
}
