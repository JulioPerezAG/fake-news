import {Routes} from '@angular/router';

import {LayoutViewComponent} from './layout-view.component';

export const routes: Routes = [{path: '', component: LayoutViewComponent}, {path: ':view', component: LayoutViewComponent}];
